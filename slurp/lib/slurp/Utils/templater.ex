defmodule Slurp.Utils.Templater do
  require Logger
  import Solid

  def send_file(file, arr \\ %{}) do
    {:ok, body} = File.read("./templates/" <> file)
    {:ok, template} = Solid.parse(body)
    Solid.render(template, arr) |> to_string |> http_html_response_header
  end

  def send_css(file) do
    {:ok, body} = File.read("./templates/" <> file)
    body |> to_string |> http_css_response_header
  end

  def send_js(file) do
    {:ok, body} = File.read("./templates/" <> file)
    body |> to_string |> http_js_response_header
  end

  def http_html_response_header(body) do
    response = """
    HTTP/1.1 200\r
    Content-Type: text/html\r
    Content-Length: #{byte_size(body)}\r
    \r
    #{body}
    """

    response
  end

  def http_css_response_header(body) do
    response = """
    HTTP/1.1 200\r
    Content-Type: text/css\r
    Content-Length: #{byte_size(body)}\r
    \r
    #{body}
    """

    response
  end

  def http_js_response_header(body) do
    response = """
    HTTP/1.1 200\r
    Content-Type: text/javascript\r
    Content-Length: #{byte_size(body)}\r
    \r
    #{body}
    """

    response
  end

  def request("get", host_name, sub_domain \\ "") do
    response = """
    GET /#{sub_domain} HTTP/1.1\r
    Host: #{host_name}\r
    \r
    """

    response

    # :unicode.characters_to_binary(String.to_charlist(response))
  end
end
