defmodule Slurp.Server.Post do
  require Logger

  def post(url, content_type) do
    postHelper(url, content_type)
  end

  defp postHelper(url, "json") do
    case {String.match?(url, ~r/^www./), :gen_tcp.connect(url, 80, [:binary])} do
      {true, {:ok, socket}} ->
        {:ok, reply} = :gen_tcp.recv(socket, 0)
        :gen_tcp.close(socket)
        reply

      {_, {_, error}} ->
        IO.puts(error)
        {:error, error}
    end
  end
end
