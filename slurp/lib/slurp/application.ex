defmodule Slurp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @doc """
   - Starts the Supervisor tree with the name 'Slurp.Supervisor'
   - By default uses the :one_for_one strategy
  """
  @impl true
  def start(_type, args) do
    IO.inspect(args)

    children = [
      {Slurp, port: 8080}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Slurp.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
