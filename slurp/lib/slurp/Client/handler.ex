defmodule Slurp.Client.Handler do
  # 'POST / HTTP/1.1\r\nHost: 127.0.0.1:8080\r\nUser-Agent: curl/7.85.0\r\nAccept: */*\r\nContent-Length: 142\r\nContent-Type: multipart/form-data; boundary=------------------------a853c35dee252f2d\r\n\r\n--------------------------a853c35dee252f2d\r\nContent-Disposition: form-data; name="name"\r\n\r\npol\r\n--------------------------a853c35dee252f2d--\r\n'

  # Having a data struct which can be used to parse the request

  defmacro __using__(_opts) do
    quote do
      @doc false
      def handle_request(socket) do
        case :gen_tcp.recv(socket, 0) do
          {:ok, reply} ->
            reply = to_string(reply)
            path = Slurp.Utils.Helper.req_target(reply)

            request_helper(socket, path)

            handle_request(socket)

          {_, _} ->
            :gen_tcp.close(socket)
            IO.puts("tcp socket closed")
        end
      end
    end
  end

  defmacro get(:default, file: file) do
    quote do
      @doc false
      def request_helper(socket, path) do
        cond do
          String.contains?(path, ".css") ->
            :gen_tcp.send(socket, Slurp.Utils.Templater.send_css(path))

          String.contains?(path, ".js") ->
            :gen_tcp.send(socket, Slurp.Utils.Templater.send_js(path))

          true ->
            IO.puts("accessing #{path}")
            :gen_tcp.send(socket, Slurp.Utils.Templater.send_file(unquote(file)))
        end
      end
    end
  end

  defmacro get(path, file: file) do
    quote do
      @doc false
      def request_helper(socket, unquote(path)) do
        IO.inspect(socket)
        IO.puts("Get request for  #{unquote(path)}")
        :gen_tcp.send(socket, Slurp.Utils.Templater.send_file(unquote(file)))
      end
    end
  end

  defmacro get(path, file: file, map: map) do
    quote do
      @doc false
      def request_helper(socket, unquote(path)) do
        IO.inspect(socket)
        IO.puts("Get request for  #{unquote(path)}")
        :gen_tcp.send(socket, Slurp.Utils.Templater.send_file(unquote(file), unquote(map)))
      end
    end
  end

  defmacro get(path, value: value) do
    quote do
      @doc false
      def request_helper(socket, unquote(path)) do
        IO.puts("Value is #{unquote(value)}")
        :gen_tcp.send(socket, Slurp.Utils.Templater.http_html_response_header(unquote(value)))
      end
    end
  end

  # get request with a function handler passed
  defmacro get(path, fun: value) do
    quote do
      # @doc false
      # def request_helper(socket, unquote(path)) do
      #   IO.puts("Value is #{unquote(value)}")
      #   :gen_tcp.send(socket, Slurp.Utils.Templater.http_html_response_header(unquote(value)))
      # end
    end
  end

  # post fun
  defmacro post(path, arr) do
    quote do
      def request_helper(socket, unquote(path)) do
        IO.puts("Post request for #{unquote(path)}")
      end
    end
  end
end
