defmodule Client.Pol do
  use Slurp.Client.Handler

  @moduledoc """
  ## About
  Introducing Slurp - an easy-to-use web framework inspired by Python's Flask. This powerful framework is designed to provide frontend engineers with a reliable platform to deploy their applications. Its fault-tolerant and scalable design ensures that your application is always running smoothly, even during heavy traffic.

  Built on Elixir's robust architecture, this framework takes advantage of its powerful concurrency features, making it an ideal choice for high-performance applications. With its minimalist design, this framework offers a simple and intuitive API, allowing developers to focus on building their applications rather than worrying about complex configurations.

  This backend framework provides the flexibility and scalability you need to make minimal web apps. Its lightweight design and easy-to-use API make it the perfect choice for frontend engineers who want to focus on building great user experiences without worrying about backend complexities.

  Benchmarking reveals that Slurp is excellent at handling concurrent requests
  ```bash
  $> siege -b 127.0.0.1:6969
  Transaction rate:     9368.61 trans/sec
  # Compared to flask's
  Transaction rate:      931.64 trans/sec
  ```

  > Note: This is not meant as a replacement for phoenix which is an industry standard for making dynamic web apps, Slurp is mainly focused on minimal applications.

  ## Config
  An example Module that provides routes for `Slurp` to use
  The Module name where routes are defined needs to be added to the `application` function inside the root level `mix.exs`

  ``` elixir
  def application do
    [
      extra_applications: [:logger],
      mod: {Slurp.Application, []},
      env: [routes: Client.Pol]
      # Add the module name to the `route` variable
    ]
  end
  ```
  Import the `Slurp.Client.Handler` module which provides function macros like `Slurp.Client.Handler.get(_route,file:_file)` expand at runtime to provide the correct config for sockets to use
  ## Examples
  ``` elixir
  defmodule Client.Pol do
  use Slurp.Client.Handler

  Slurp.Client.Handler.get("/", file: "index.html")
  #Creates a route on the endpoing `/` to send back `index.html`
  Slurp.Client.Handler.get(:default, file: "index.html")
  #Creates a default get route which sends `index.html` if none of the previous defined routes are matched
  #NOTE: defaullt case should always be the last case
  end
  ```
  The files that are being used in `Slurp.Client.Handler.get(_route,file:_file)` should be defined in the templates folder

  The directory configuration should look similiar too
  ``` bash
  .
  ├── lib
  │   └── main.ex
  ├── mix.exs
  ├── slurp
  │   ├── lib
  │   ├── mix.exs
  │   └── test
  └── templates
    └── index.html
  ```
  """

  Slurp.Client.Handler.get("/", file: "index.html", map: %{"Kair" => "Kair"})

  # Slurp.Client.Handler.get("/random", value: random_number)
  Slurp.Client.Handler.get(:default, file: "index.html")

  @doc false
  def random_number() do
    to_string(:rand.uniform(100))
  end
end
