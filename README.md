# Slurp

Slurp is a robust, scalable, and developer-friendly backend framework written in Elixir. It leverages the power of the Actor model and the Erlang VM to provide a high-performance, fault-tolerant foundation for building modern web applications and APIs.

## Features

- Scalable: Harness the power of the Erlang VM for highly concurrent and distributed systems.
- Modular: A plug-based architecture allows for easy customization and extension.
- Developer Friendly: Enjoy a great developer experience with easy to use macros.


## Example

``` elixir
# add dynamic content to pages 
  Slurp.Client.Handler.get("/random", value: random_number)

# template in place using a templating engine 
  Slurp.Client.Handler.get("/", file: "index.html", map: %{"Kair" => "Kair"})

# set default pages
  Slurp.Client.Handler.get(:default, file: "index.html")



  @doc false
  def random_number() do
    to_string(:rand.uniform(100))
  end
end

```
for more [read](https://gitlab.com/HolyChicken99/slurp/-/blob/main/lib/main.ex?ref_type=heads) 


### Benchmark

Slurp can handle 10x more requests than python's flask
![benchmark](./slurp/assets/Screenshot_56.png)



### [Documentation](https://holychicken99.gitlab.io/slurp/api-reference.html)

